var express = require('express');

var router = express.Router();

var requestMOD = require('./requestMOD/requestMOD.router');
var userMOD = require('./userMOD/userMOD.router');
//var otherMOD = require('./otherMOD/otherMOD.router');


router.get('/', function(req, res) {
  res.send('Welcome to API Front Page');
});

router.use('/request', requestMOD);
router.use('/user', userMOD);
//router.use('/other', otherMOD);



exports.router = router;
