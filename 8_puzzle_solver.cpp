#include <iostream>
#include <queue>
#include <stack>
using namespace std;

class state
{
public:
	int board[3][3];
	state *parent;

	state(int arr[3][3])
	{
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				board[i][j] = arr[i][j];
	}
	bool issol()
	{
		int temp[3][3] = {1,2,3,4,5,6,7,8,0};
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				if (board[i][j] != temp[i][j])return false;
		return true;
	}
	
	void display()
	{
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				cout << board[i][j] << "  ";
			}
			cout << endl;
		}
	}

	bool operator ==(state* s)
	{
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				if (this->board[i][j]!=s->board[i][j])return false;
		return true;
	}
	bool operator !=(state* s)
	{
		if (s == NULL)return true;
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				if (this->board[i][j] != s->board[i][j])return true;
		return false;
		
	}
};

void swap(int &x, int &y)
{
	int z;
	z = x;
	x = y;
	y = z;
}
void main()
{
	cout << "enter the first state\n";
	int arr[3][3];
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			cin >> arr[i][j];
	cout << "solving....\n";
	int co = 0;

	queue <state*> q;
	state* first = new state(arr);
	//first->display();
	first->parent = NULL;
	q.push(first);
	state* current;
	while (!q.empty())
	{
		co++;
		//cout << co << endl;
		current = q.front();
		//current->display();
		//cout << endl << "===========================" << endl;
		q.pop();
		if (current->issol())
			break;
		else {//make children
			int row, col;
			int t_row, t_col;
			int temp[3][3];

			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					temp[i][j] = current->board[i][j];
			//search for the free slot
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					if (temp[i][j] == 0){ row = i; col = j; }
				}
			}
			//row,col+1
			t_row = row; t_col = col + 1;
			if (t_col < 3)
			{
				swap(temp[row][col], temp[t_row][t_col]);
				
				state* child = new state(temp);
				if ((*child != (current->parent)))
				{
					child->parent = current;
					q.push(child);
				}
				else{ delete child; }

				swap(temp[row][col], temp[t_row][t_col]);
			}

			//row,col-1
			t_row = row; t_col = col - 1;
			if (t_col >= 0)
			{
				swap(temp[row][col], temp[t_row][t_col]);
				state* child = new state(temp);
				
				if ((*child != (current->parent)))
				{
					child->parent = current;
					q.push(child);
				}
				else{ delete child; }

				swap(temp[row][col], temp[t_row][t_col]);
			}
			//row+1,col
			t_row = row+1; t_col = col;
			if (t_row < 3)
			{
				swap(temp[row][col], temp[t_row][t_col]);
				state* child = new state(temp);
				if ((*child != (current->parent)))
				{
					child->parent = current;
					q.push(child);
				}
				else{ delete child; }

				swap(temp[row][col], temp[t_row][t_col]);
			}

			//row-1,col
			t_row = row-1; t_col = col;
			if (t_row>=0)
			{
				swap(temp[row][col], temp[t_row][t_col]);
				state* child = new state(temp);
				if ((*child != (current->parent)))
				{
					child->parent = current;
					q.push(child);
				}
				else{ delete child; }

				swap(temp[row][col], temp[t_row][t_col]);
			}
		}

		
	}


	stack <state*> s;
	int c=0;
	while (current->parent != NULL)
	{
		s.push(current);
		current = current->parent;
		c++;
	}

	s.push(current);


	while (!s.empty())
	{
		state* curr = s.top();
		curr->display();
		cout << endl << "===========================" << endl;
		s.pop();
	}


	cout << "number of moves = " << c <<"  using exam = "<<co<< endl;

}