var express = require('express');
var router = express.Router();

var admin = require('./adminMOD.controller');
// hwa el byktb fi el path da path file controller sah



// hwa law galo / admin bas m3nha edkhol file admin sah ? w s3tha di el whida el bt3mel routing lma yb2a dkhel file w m3hosh routing tani
router.route('/admin')
      .post(admin.addNewAdmin);//done

router.route('/admin/storage') // fiha haga law khalit el endpoints /storage 3ala tol wela 3shan admen en mfish fi heta nfs endpoint di had 3melha
      .post(admin.addNewStorage)//done
      .get(admin.viewStorage); // done
router.route('/admin/employees')
       .post (admin.addNewEmployee)//done
       .get(admin.viewEmployee); //done
  router.route('/admin/mealRequest')
       .get(admin.viewMealRequest); //done
   router.route('/admin/moneyRequest')
         .get(admin.viewMoneyRequest);//done
  router.route('/admin/poorFamilies')
        .get(admin.viewPoorFamilies);//done
router.route('/admin/users')
      .get(admin.viewAllUsers); //done
  router.route('/admin/volunteers')
        .get(admin.viewVolunteers);//done
//router.route('/admin/changePassword')
      //.post(admin.changePassword); // not sure menha put wela post

router.route('/admin/donor')
       .get(admin.viewDonors);//done


  router.route('/admin/employees/:id')
        .get(admin.deleteEmployee)
  router.route('/admin/poorFamilies/:id')
        .get(admin.deletePoorFamily)

router.route('/admin/count/user')
      .get(admin.countUser) //done
router.route('/admin/count/donor')
      .get(admin.countDonor) //done
router.route('/admin/count/volunteer')
       .get(admin.countVolunteer) //done
router.route('/admin/count/employee')
       .get(admin.countEmployee)//done
router.route('/admin/count/mealRequest/:state')
       .get(admin.countMealRequest)//done
router.route('/admin/count/moneyRequest/:state')
      .get(admin.countMoneyRequest) //DONE



router.route('/admin/count/maxNumOfDonations')
      .get(admin.maxNumOfDonations) //done bs lsa 7tet user
router.route('/admin/count/minNumOfDonations')
      .get(admin.minNumOfDonations) //done bs lsa 7tet user 


  module.exports = router;
