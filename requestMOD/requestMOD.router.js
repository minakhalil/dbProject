var express = require('express');
var router = express.Router();

var reqModController = require('./requestMOD.controller');
var u= require('../userMOD/userMOD.controller');

router.route('/meal')
      .post(u.isLogin,reqModController.addNewRequest); // donar make new req

      router.route('/meal/all/')
            .get(reqModController.getAllReq); // a get kol lly req lly f loc mo3ayan

router.route('/meal/:id')
      .get(reqModController.getReqData) // a get l data bta3t req elly l id bta3o
      .put(reqModController.updateReqData);


router.route('/meal/gen/loc/:loc')
      .get(reqModController.poorReq)
      .put(reqModController.acceptPoorReq);
      //send V2

  router.route('/money/')
        .post(reqModController.addNewMoneyRequest); // donar make new money req
  router.route('/money/:id')
        .get(reqModController.getReqMoneyData) // when the user wants to check his request or edit them
        .put(reqModController.updateReqMoneyData);
 router.route('/money/loc/:loc')
      .get(reqModController.getAllMoneyReq); // when he emp wants to see mony request sorted with location




module.exports = router;
