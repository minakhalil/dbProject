-- MySQL dump 10.13  Distrib 5.7.16, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: NoHunger
-- ------------------------------------------------------
-- Server version	5.7.16-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Donor`
--

DROP TABLE IF EXISTS `Donor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Donor` (
  `donorID` int(11) NOT NULL,
  `numberOfDonations` int(11) NOT NULL DEFAULT '0',
  `lastDonations` date DEFAULT NULL,
  PRIMARY KEY (`donorID`),
  CONSTRAINT `donor to user` FOREIGN KEY (`donorID`) REFERENCES `User` (`userID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Donor`
--

LOCK TABLES `Donor` WRITE;
/*!40000 ALTER TABLE `Donor` DISABLE KEYS */;
INSERT INTO `Donor` VALUES (2,0,'2016-12-06'),(4,3,'2016-12-06');
/*!40000 ALTER TABLE `Donor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Employee`
--

DROP TABLE IF EXISTS `Employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Employee` (
  `age` int(11) NOT NULL,
  `empID` int(11) NOT NULL,
  UNIQUE KEY `empID_UNIQUE` (`empID`),
  CONSTRAINT `Employee to User` FOREIGN KEY (`empID`) REFERENCES `User` (`userID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Employee`
--

LOCK TABLES `Employee` WRITE;
/*!40000 ALTER TABLE `Employee` DISABLE KEYS */;
INSERT INTO `Employee` VALUES (21,3);
/*!40000 ALTER TABLE `Employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MealRequest`
--

DROP TABLE IF EXISTS `MealRequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MealRequest` (
  `requestID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `donorID` int(11) DEFAULT NULL,
  `pickupTime` datetime DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  `location` int(11) NOT NULL,
  `expirationDate` date DEFAULT NULL,
  `numberOfMeals` int(11) NOT NULL,
  `notes` longtext,
  `volunteerID` int(11) DEFAULT NULL,
  `poorID` int(11) DEFAULT NULL,
  PRIMARY KEY (`requestID`),
  UNIQUE KEY `requestID_UNIQUE` (`requestID`),
  KEY `Meal to Donor (User)_idx` (`donorID`),
  KEY `Meal to Poor_idx` (`poorID`),
  KEY `Meal to Volunteer1 (User)_idx` (`volunteerID`),
  KEY `meal to storage_idx` (`location`),
  CONSTRAINT `Meal to Poor` FOREIGN KEY (`poorID`) REFERENCES `Poor` (`poorID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `meal to donor` FOREIGN KEY (`donorID`) REFERENCES `Donor` (`donorID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `meal to storage` FOREIGN KEY (`location`) REFERENCES `StorageFood` (`storageID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `meal to volunteer` FOREIGN KEY (`volunteerID`) REFERENCES `Volunteer` (`volunteerID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MealRequest`
--

LOCK TABLES `MealRequest` WRITE;
/*!40000 ALTER TABLE `MealRequest` DISABLE KEYS */;
INSERT INTO `MealRequest` VALUES (1,'asjd',2,'2015-02-02 15:15:15',3,2,'2015-01-03',5,NULL,1,NULL),(2,'sda',2,'2016-02-02 15:15:15',2,1,'2016-02-01',7,NULL,1,NULL),(12,NULL,NULL,NULL,1,1,NULL,2,NULL,NULL,1),(13,NULL,NULL,NULL,1,1,NULL,2,NULL,NULL,1),(14,NULL,NULL,NULL,1,1,NULL,2,NULL,NULL,1),(15,NULL,NULL,NULL,1,1,NULL,2,NULL,NULL,1),(16,NULL,NULL,NULL,1,1,'2015-01-03',2,NULL,NULL,1),(17,NULL,NULL,NULL,1,1,'2015-01-03',2,NULL,NULL,1),(18,NULL,NULL,NULL,1,2,'2015-01-03',20,NULL,NULL,3),(19,NULL,NULL,NULL,1,2,'2015-01-03',20,NULL,NULL,3),(20,NULL,NULL,NULL,1,2,'2015-01-03',20,NULL,NULL,3),(21,NULL,4,'2015-02-02 20:20:20',1,1,'2016-07-07',30,'jshdasdaskdj',NULL,NULL),(22,NULL,4,'2015-02-02 20:20:20',1,1,'2016-07-07',30,'jshdasdaskdj',NULL,NULL);
/*!40000 ALTER TABLE `MealRequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MoneyRequest`
--

DROP TABLE IF EXISTS `MoneyRequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MoneyRequest` (
  `moneyRequestID` int(11) NOT NULL AUTO_INCREMENT,
  `state` int(11) NOT NULL DEFAULT '1',
  `PickupTime` datetime DEFAULT NULL,
  `donation` int(11) NOT NULL,
  `donorID` int(11) NOT NULL,
  `empID` int(11) DEFAULT NULL,
  PRIMARY KEY (`moneyRequestID`),
  UNIQUE KEY `moneyRequest_UNIQUE` (`moneyRequestID`),
  KEY `Money to Donor (User)_idx` (`donorID`),
  KEY `Money to Employee (User)_idx` (`empID`),
  CONSTRAINT `Money to Donor (User)` FOREIGN KEY (`donorID`) REFERENCES `Donor` (`donorID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Money to Employee (User)` FOREIGN KEY (`empID`) REFERENCES `Employee` (`empID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MoneyRequest`
--

LOCK TABLES `MoneyRequest` WRITE;
/*!40000 ALTER TABLE `MoneyRequest` DISABLE KEYS */;
INSERT INTO `MoneyRequest` VALUES (4,1,'2013-08-05 18:19:03',20,2,NULL),(5,1,'2013-08-05 18:19:03',50,2,NULL),(6,1,'2013-08-05 18:19:03',50,2,NULL),(7,1,'2013-08-05 18:19:03',50,2,NULL),(8,1,'2013-08-05 18:19:03',50,2,NULL);
/*!40000 ALTER TABLE `MoneyRequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Poor`
--

DROP TABLE IF EXISTS `Poor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Poor` (
  `poorID` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `numberOfPersons` int(11) NOT NULL,
  `location` int(11) NOT NULL,
  `buildingNo` varchar(45) NOT NULL,
  `street` varchar(80) NOT NULL,
  `city` varchar(45) NOT NULL,
  `employeeID` int(11) NOT NULL,
  `lastDonationRecieved` date NOT NULL,
  PRIMARY KEY (`poorID`),
  UNIQUE KEY `poorID_UNIQUE` (`poorID`),
  KEY `fk_Poor_1Poor to Employee (User)_idx` (`employeeID`),
  KEY `poor to storage_idx` (`location`),
  CONSTRAINT `Poor to Employee (User)` FOREIGN KEY (`employeeID`) REFERENCES `Employee` (`empID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `poor to storage` FOREIGN KEY (`location`) REFERENCES `StorageFood` (`storageID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Poor`
--

LOCK TABLES `Poor` WRITE;
/*!40000 ALTER TABLE `Poor` DISABLE KEYS */;
INSERT INTO `Poor` VALUES (1,'jsad',2,1,'2','sd1','cairo',3,'2016-12-05'),(2,'bisho',5,1,'2','sdak','cairo',3,'2016-12-05'),(3,'khlil',20,2,'423','asd','sda',3,'2016-12-05');
/*!40000 ALTER TABLE `Poor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StorageFood`
--

DROP TABLE IF EXISTS `StorageFood`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StorageFood` (
  `storageID` int(11) NOT NULL,
  `storageName` varchar(45) NOT NULL,
  `maxCapacity` int(11) DEFAULT '0',
  `currentCapacity` int(11) DEFAULT '0',
  `buildingNO` varchar(15) NOT NULL,
  `street` varchar(15) NOT NULL,
  `city` varchar(15) NOT NULL,
  PRIMARY KEY (`storageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StorageFood`
--

LOCK TABLES `StorageFood` WRITE;
/*!40000 ALTER TABLE `StorageFood` DISABLE KEYS */;
INSERT INTO `StorageFood` VALUES (1,'dokki',0,0,'12','cairo','cairo'),(2,'fisal',0,0,'13','ad','adsd');
/*!40000 ALTER TABLE `StorageFood` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `type` int(11) NOT NULL,
  `password` varchar(80) NOT NULL,
  `picture` varchar(80) DEFAULT 'han7ot link sora mo3iana',
  `buildingNo` varchar(15) NOT NULL,
  `city` varchar(15) NOT NULL,
  `street` varchar(15) NOT NULL,
  `location` int(11) NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `UserID_UNIQUE` (`userID`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `Email_UNIQUE` (`email`),
  KEY `fk_User_1_idx` (`location`),
  CONSTRAINT `user to storage` FOREIGN KEY (`location`) REFERENCES `StorageFood` (`storageID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'Dummy1','sadhsd','0123456789',1,'ai7aga','ai7aga','1','cairo','cairo',0),(2,'khalil','asd','0123456789',1,'ai7aga','ai7aga','1','cairo','cairo',0),(3,'Dummy3','sdawwq','0123456789',2,'a7aga','ai7aga','1','cairo','cairo',0),(4,'gg','ggg','123123',2,'asda','sada','2','sda','sad',1);
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Volunteer`
--

DROP TABLE IF EXISTS `Volunteer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Volunteer` (
  `volunteerID` int(11) NOT NULL,
  `maxKilos` int(11) DEFAULT '10',
  UNIQUE KEY `volunteerID_UNIQUE` (`volunteerID`),
  CONSTRAINT `Volunteer to User` FOREIGN KEY (`volunteerID`) REFERENCES `User` (`userID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Volunteer`
--

LOCK TABLES `Volunteer` WRITE;
/*!40000 ALTER TABLE `Volunteer` DISABLE KEYS */;
INSERT INTO `Volunteer` VALUES (1,NULL);
/*!40000 ALTER TABLE `Volunteer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-06 14:32:16
