var express = require('express');

var router = express.Router();

var bassemMod = require('./bassemMod/bassemMod.router');

router.get('/', function(req, res) {
  res.send('Welcome to API Front Page');
});

router.use('/bassemMod', bassemMod);
//router.use('/brendaMob', brendaMob);
//router.use('/bishoMod', bishoMod);



exports.router = router;
