var express = require('express');
var app = express();
var bodyParser = require('body-parser');
session = require('express-session');
var con = require('./config.js');


/**
  * Add body parser
  */

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/**
  * Add session secret
  */
app.use(session({
    secret: 'kj54ddBishoElMonofy54354',
    resave: true,
    saveUninitialized: true
}));

var routes = require('./routes');


app.listen(4000);


/**
  * API URL
  */
app.use('/api', routes.router);
