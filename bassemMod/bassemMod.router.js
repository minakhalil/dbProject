var express = require('express');
var router = express.Router();

var bassemModController = require('./bassemMod.controller');

router.route('/')
      .get(bassemModController.func1)
      .post(bassemModController.func2);

      router.route('/bisho/name')
            .get(bassemModController.func1)
            .post(bassemModController.func2);
module.exports = router;
