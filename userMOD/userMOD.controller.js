var con = require('../config.js');
//var bcrypt = require('bcrypt');
//var salt = bcrypt.genSaltSync(10);

var salt = 10;


exports.login	 =function(req, res){

console.log(req.body);
  con.query("SELECT * From User left outer join Donor on Donor.donorID=User.userID left outer join Volunteer on Volunteer.volunteerID=User.userID left outer join Employee on Employee.empID= User.userID where  User.name=? AND password = ?;",[req.body.username,/*bcrypt.hashSync(*/req.body.password/*, salt)*/],function(err,rows){
    var resJ={}

    if (rows.length == 0) {
                    resJ.succ = null;
                   resJ.err = "Wrong username or password";
                   res.end(JSON.stringify(resJ));

               }else {
                req.session.user = JSON.parse(JSON.stringify(rows));
                resJ.err = null;


                resJ.succ = JSON.parse(JSON.stringify(rows));
                delete resJ.succ[0].password;
                delete resJ.succ[0].volunteerID;
                delete resJ.succ[0].empID;
                delete resJ.succ[0].lastDonations;
                delete resJ.succ[0].donorID;



                res.end(JSON.stringify(resJ));

            }

              });

};


exports.addVolunteer	 =function(req, res){

  con.query("INSERT  INTO Volunteer (volunteerID,maxKilos) Values(?,?) ;",[req.body.userid,req.body.maxkilos],function(err,rows){
  var resJ={}
//console.log(err);

resJ.err = err;
resJ.succ = rows;


res.end(JSON.stringify(resJ));


              });

};


exports.addDonor	 =function(req, res){
  var resJ={}
  console.log(req.body);
  con.query("INSERT  INTO Donor (donorID,city,street,buildingNo) Values(?,?,?,?) ;",[req.body.userid,req.body.city,req.body.street,req.body.buildingno],function(err,rows){

//console.log(err);

resJ.err = err;
resJ.succ = rows;


res.end(JSON.stringify(resJ));


              });

};


exports.signup	 =function(req, res){


  con.query("SELECT  name From User WHERE name = ? ;",[req.body.username],function(err,rows){
  var resJ={}
console.log(rows);
    if (rows.length != 0) {
                    resJ.succ = null;
                   resJ.err = "Username taken.";

                   res.end(JSON.stringify(resJ));

               }
               else{
console.log("NO");
    con.query("SELECT  email From User WHERE email = ? ;",[req.body.email],function(err,rows){

      if (rows.length != 0) {
                      resJ.succ = null;
                     resJ.err = "Email taken.";

                     res.end(JSON.stringify(resJ));

                 }
                 else{


      con.query("INSERT  INTO User (name,password,email,phone,type,location) Values(?,?,?,?,?,?) ;",[req.body.username,/*bcrypt.hashSync(*/req.body.password/*, salt)*/,req.body.email,req.body.phone,req.body.type,req.body.storage],function(err,rows){

//console.log(err);




con.query("SELECT  userID From User WHERE name = ? ;",[req.body.username],function(err,rows){

//console.log(err);




              resJ.err = err;
              resJ.succ = rows;


              res.end(JSON.stringify(resJ));






            });




                  });

}

                });


}
              });

};

exports.dashboard	 =function(req, res){
  con.query("SELECT * From User left outer join Donor on Donor.donorID=User.userID left outer join Volunteer on Volunteer.volunteerID=User.userID left outer join Employee on Employee.empID= User.userID where  User.userID=?;",[req.session.user[0].userID],function(err,rows){
    var resJ={}
resJ.err=null;
resJ.succ=JSON.stringify(rows);
delete resJ.succ[0].password;
delete resJ.succ[0].volunteerID;
delete resJ.succ[0].empID;
delete resJ.succ[0].lastDonations;
delete resJ.succ[0].donorID;



                res.end(JSON.stringify(resJ));



              });


};



exports.logout	 =function(req, res){

var resJ={}

    req.session.destroy();
      resJ.err = null;
      resJ.succ = "logged out!";
      res.end(JSON.stringify(resJ));

};
exports.isLogin =function(req, res, next) {
  if (!req.session.user) {
    res.status(401).send("You are not logged in");
  } else {
    next();
    return;
  }
}
