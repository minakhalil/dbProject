var express = require('express');
var router = express.Router();

var userModController = require('./userMOD.controller');

router.route('/login')
      .post(userModController.login);

router.route('/signup')
     .post(userModController.signup);

router.route('/addvolunteer')
    .post(userModController.addVolunteer);

    router.route('/adddonor')
        .post(userModController.addDonor);


router.route('/dashboard')
      .get(userModController.isLogin,userModController.dashboard);

router.route('/logout')
      .get(userModController.logout);

module.exports = router;
